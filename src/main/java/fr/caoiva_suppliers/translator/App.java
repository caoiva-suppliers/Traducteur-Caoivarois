/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator;

import java.io.File;
import java.io.IOException;

import javax.swing.filechooser.FileSystemView;

import fr.caoiva_suppliers.translator.modele.Traducteur;
import fr.caoiva_suppliers.translator.view.callahan.CallahanView;
import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application {

	private static String externContentPath;

	private static File mainFolder; // /Traducteur Caoivarois

	private static String internContentFolderPath; // war/content
	private static String internImagesFolderPath; //

	private static String internPakFolderPath;
	private static String internFileExportFolderPath;

	private static App instance;

	public static App getInstance() {
		if (instance == null)
			instance = new App();
		return instance;
	}

	@Override
	public void start(Stage stage) throws IOException, InterruptedException {
		final Traducteur trad = new Traducteur();
		new CallahanView(trad);
	}

	public static void main(String[] args) {
		final FileSystemView fsv = FileSystemView.getFileSystemView();
		mainFolder = new File(fsv.getDefaultDirectory().getPath() + File.separator + "Traducteur Caoivarois");
		internContentFolderPath = "data" + File.separator + "Content";
		internPakFolderPath = "packer" + File.separator + "pak";
		internFileExportFolderPath = "packer" + File.separator + "War" + File.separator;

		launch(args);
	}

	public String getInternContentFolderPath() {
		return internContentFolderPath;
	}

	public String getInternImageFolderPath() {
		return internImagesFolderPath;
	}

	public String getInternPakFolderPath() {
		return internPakFolderPath;
	}

	public String getExternContentPath() {
		return externContentPath;
	}

	public String getInternFileExportFolderPath() {
		return internFileExportFolderPath;
	}

	public void setExternContentPath(String externContentPath) {
		App.externContentPath = externContentPath;
	}

	public File getMainFolder() {
		return mainFolder;
	}

	public void about() {
		this.getHostServices().showDocument("https://gitlab.com/caoiva-suppliers/Traducteur-Caoivarois");
	}

}
