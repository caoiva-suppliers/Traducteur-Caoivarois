/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.filesManagment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import fr.caoiva_suppliers.translator.App;
import fr.caoiva_suppliers.translator.modele.ItemPickup;
import fr.caoiva_suppliers.translator.modele.Traduction;

public final class FileManager {

	private static FileManager instance;

	private FileManager() {
	}

	public static FileManager getInstance() {
		if (instance == null)
			instance = new FileManager();
		return instance;
	}

	public byte[] readFile(Path path) {

		try {
			byte[] data = Files.readAllBytes(path);
			return data;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new byte[0];

	}

	public void writeFiles(Traduction trad) {
		final Path internItemPickup = Paths.get(App.getInstance().getInternContentFolderPath() + File.separator
				+ "Blueprints" + File.separator + "ItemPickups");

		final Path warFolder = Paths.get(App.getInstance().getInternFileExportFolderPath());

		try (DirectoryStream<Path> stream = Files.newDirectoryStream(internItemPickup)) {
			if (Files.exists(warFolder))
				Files.walk(warFolder).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);

			Files.createDirectories(warFolder);

			replaceContent(trad, warFolder, stream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void writeFile(Path path, byte[] content) {
		try {
			Files.write(path, content);
		} catch (FileNotFoundException e) {
			System.out.println("le fichier " + path + " n'a pas pu être écris"); // TODO
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void replaceContent(Traduction trad, Path warFolder, DirectoryStream<Path> stream) throws IOException {
		Iterator<Path> paths = stream.iterator();
		Iterator<ItemPickup> items = trad.getItemList().iterator();

		Path file = paths.next();

		while (items.hasNext()) {
			ItemPickup item = items.next();
			if (!item.isTtlTrnsltd() && !item.isDscTrnsltd())
				continue;

			boolean stop = false;
			do {

				if (item.getName().equals(removeUasset(file.getFileName().toString()))) {
					byte[] content = readFile(file);
					if (item.isTtlTrnsltd()) {
						content = replaceTitle(item, content); // TODO
					}
					if (item.isDscTrnsltd()) {
						content = replaceDesc(item, content); // TODO
					}
					Path translated = Paths.get(warFolder.toString() + File.separator + "Content" + File.separator
							+ item.getPath() + ".uasset");
					if (!Files.exists(translated.getParent()))
						Files.createDirectories(translated.getParent());

					writeFile(translated, content);
					stop = true;
				} else if (paths.hasNext()) {
					file = paths.next();
				} else
					stop = true;
			} while (!stop);

		}
	}

	private byte[] replaceTitle(ItemPickup temp, byte[] data) throws UnsupportedEncodingException {

		for (int i = 0; i < temp.getTitleSize(); i++) {
			byte[] translation = temp.getTradTitle().getBytes("windows-1252");

			if (i < temp.getTradTitle().length())
				data[temp.getTitlePos() + i] = translation[i];
			else
				data[temp.getTitlePos() + i] = '\0';
		}
		return data;
	}

	private byte[] replaceDesc(ItemPickup temp, byte[] data) throws UnsupportedEncodingException {

		for (int i = 0; i < temp.getDescSize(); i++) {
			byte[] translation = temp.getTradDesc().getBytes("windows-1252");

			if (i < temp.getTradDesc().length())
				data[temp.getDescPos() + i] = translation[i];
			else
				data[temp.getDescPos() + i] = '\0';
		}
		return data;
	}

	public List<ItemPickup> loadInternBlueprint() {
		final ArrayList<ItemPickup> itemList = new ArrayList<>();
		final Path internItemPickup = Paths.get(App.getInstance().getInternContentFolderPath() + File.separator
				+ "Blueprints" + File.separator + "ItemPickups");
		loadInternItem(internItemPickup, itemList);
		return itemList;
	}

	public void loadInternItem(Path internItemPickup, List<ItemPickup> itemList) {

		try (DirectoryStream<Path> stream = Files.newDirectoryStream(internItemPickup)) {
			for (final Path file : stream) {
				if (!Files.isDirectory(file)) {
					final ItemPickup item = new ItemPickup(removeUasset(file.getFileName().toString()),
							removePartialPath(removeUasset(file.toString())));
					byte data[] = readFile(file);
					String content = new String(data, "windows-1252");
					item.setContent(content);

					itemList.add(item);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String removePartialPath(String path) {
		final int debut = path.lastIndexOf(File.separator + "Content" + File.separator);
		return path.substring(debut + 9);
	}

	public String removeUasset(String toRemove) {
		final String res = toRemove.replaceAll(".uasset", "");
		return res;
	}

}
