/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.filesManagment;

import java.io.IOException;

public class Paker {

	private static Paker instance;

	private Paker() {
	}

	public static Paker getInstance() {
		if (instance == null)
			instance = new Paker();
		return instance;
	}

	public void unpak() {

	}

	public void pak() {
		if (System.getProperty("os.name").contains("Windows"))
			try {
				windowsPak();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
	}

	private void windowsPak() throws IOException, InterruptedException {
		// TODO
	}

}
