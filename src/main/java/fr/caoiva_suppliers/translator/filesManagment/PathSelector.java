/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.filesManagment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.caoiva_suppliers.translator.App;
import fr.caoiva_suppliers.translator.view.callahan.TraducteurController;
import javafx.application.Platform;
import javafx.stage.DirectoryChooser;

public class PathSelector {

	public void pathSelector(TraducteurController stage) { // sert a choisir des données externe

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		DirectoryChooser fileChooser = new DirectoryChooser();
		fileChooser.setTitle("Selectionnez le dossier Content");
		File res = fileChooser.showDialog(stage);

		new Thread(new Runnable() {
			@Override
			public void run() {
				if (res != null) {
					System.out.println("Voici le chemin du \\War: " + res.getPath()); // TODO
					App.getInstance().setExternContentPath(res.getPath());
					try {
						copyTree();
					} catch (IOException e) {
						e.printStackTrace();
					}

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							stage.reloadTraductions();
						}
					});
				}
			}
		}).start();

	}

	public void copyTree() throws IOException { // remplace les données internes par les externes

		// final String blueprintPath = App.getMainFolder().getPath() + File.separator +
		// "data" + File.separator + "War"
		// + File.separator + "Content" + File.separator + "Blueprints";

		final String blueprintPath = App.getInstance().getInternContentFolderPath() + File.separator + "Blueprints";

		File dest = new File(blueprintPath);
		dest.mkdirs();

		final File src = new File(App.getInstance().getExternContentPath() + File.separator + "Blueprints");

		System.out.println(src.exists());
		System.out.println(src.isDirectory());

		copyFiles(src, dest);

		removeEmpty(dest);

		Files.newDirectoryStream(Paths.get(dest.getPath())).forEach(System.out::println);

	}

	public void copyFiles(File from, File to) throws IOException { // sert à copier un dossier vers un autre
		System.out.println("from " + from); // tout en checkant la validité
		System.out.println("to   " + to);
		File files[] = from.listFiles();
		for (final File src : files) {

			final File dest = new File(to.getPath() + File.separator + src.getName());

			if (src.isDirectory()) {
				// if(hasChild(f)) {
				dest.mkdirs();
				copyFiles(src, dest);
				// }
			} else {
				System.out.println("Fichier: " + src);
				System.out.println("  -> " + containsTextField(src));
				if (containsTextField(src)) {
					if (dest.exists())
						dest.delete();
					Files.copy(src.toPath(), dest.toPath());
				}
			}
		}
	}

	public boolean hasChild(File f) {
		final File files[] = f.listFiles();
		if (files.length == 0)
			return false;
		int idx = 0;
		while (idx < files.length) {
			if (files[idx].isFile())
				return true;
			else if (hasChild(files[idx]))
				return true;
			idx++;
		}
		return false;
	}

	public void removeEmpty(File f) { // fonction qui enleve les dossiers vides
		if (f.isDirectory()) {
			if (!hasChild(f)) {
				f.delete();
			} else {
				final File files[] = f.listFiles();
				for (final File child : files) {
					removeEmpty(child);
				}
			}
		}

	}

	public boolean containsTextField(File f) throws IOException {
		boolean checked = false;
		int idx = 0;

		final StringBuilder sb = new StringBuilder();
		final BufferedReader b = new BufferedReader(new FileReader(f));// TODO
		String readLine;
		while ((readLine = b.readLine()) != null) {
			sb.append(readLine);
		}
		b.close();

		final String content = sb.toString();

		while (idx + 32 < content.length() && !checked) {
			idx++;

			Character letter = content.charAt(idx);

			if (letter.toString().matches("[0-9A-F]")) {

				final String cc = content.substring(idx + 1, idx + 32);
				if (cc.matches("[0-9A-F]*")) {
					checked = true;
				} else {
					for (int i = 0; i < cc.length(); i++) {
						letter = cc.charAt(i);
						if (!letter.toString().matches("[0-9A-F]")) {
							idx += i;
							break;
						}
					}
				}

			}
		}
		return checked;
	}
}
