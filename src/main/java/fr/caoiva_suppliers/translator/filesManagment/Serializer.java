/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.filesManagment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import fr.caoiva_suppliers.translator.modele.Traduction;

public class Serializer {

	public final static String DATAPATH = "data" + File.separator + "traduction" + File.separator;

	public static void writeTrad(Traduction trad) {

		ObjectOutputStream oos = null;
		System.out.println(trad.getName() + " enregistré");
		makeContainerFolder();

		try {
			final FileOutputStream fichier = new FileOutputStream(DATAPATH + trad.getName() + ".trad");
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(trad);
		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private static void makeContainerFolder() {
		final File traduction = new File(DATAPATH);
		if (traduction.exists()) {
			if (traduction.isFile()) {
				traduction.delete();
				traduction.mkdirs();
			}
		} else
			traduction.mkdirs();
	}

	public static List<Traduction> getTrads() {
		final File traduction = new File("data" + File.separator + "traduction" + File.separator);
		File[] temporaires = new File[0];
		final ArrayList<Traduction> traductions = new ArrayList<>();
		if (traduction.exists() && traduction.isDirectory()) {
			temporaires = traduction.listFiles();
		}

		for (final File f : temporaires) {
			if (!(f.isFile() && f.getPath().substring(f.getPath().length() - 5, f.getPath().length()).equals(".trad")))
				;
			traductions.add(getTrad(f));
		}
		return traductions;
	}

	private static Traduction getTrad(File f) {

		ObjectInputStream ois = null;
		Traduction trad = null;
		try {
			final FileInputStream fichier = new FileInputStream(f);
			ois = new ObjectInputStream(fichier);
			trad = (Traduction) ois.readObject();
			System.out.println(f.getPath()); // TODO
		} catch (final IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
				return trad;
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

}
