/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.modele;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Blueprint implements Serializable {

	private String name;
	private String fileName;

	public Blueprint(String name, String path) {
		this.name = name;
		this.fileName = path;
	}

	public Blueprint(String path) {
		this.fileName = path;
		setName(removePath(path));
	}

	public String removePath(String path) {
		int idx = path.length();
		while (path.charAt(idx) != '\\' && idx >= 0) {
			idx--;
		}
		int idfin = idx;
		do {
			while (idfin != '.' && idfin <= path.length()) {
				idfin++;
			}
		} while (!".uasset".equals(path.substring(idfin + 1)));
		return path.substring(idx, idfin + 1);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return fileName;
	}

	public void setPath(String path) {
		this.fileName = path;
	}

}
