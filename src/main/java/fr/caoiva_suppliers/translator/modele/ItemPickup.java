/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.modele;

import java.io.File;
import java.nio.charset.StandardCharsets;

@SuppressWarnings("serial")
public class ItemPickup extends Blueprint {

	private String title;
	private String desc;
	private int titleSize;
	private int titlePos;
	private int descSize;
	private int descPos;
	private String tradTitle;
	private String tradDesc;
	private String imagePath;

	private boolean isTtlTrnsltd;
	private boolean isDscTrnsltd;

	public ItemPickup(String name, String path) {
		super(name, path);
	}

	public ItemPickup(String path) {
		super(path);
	}

	public boolean setContent(String content) {
		isTtlTrnsltd = false;
		isDscTrnsltd = false;
		int idx = -1;

		setImagePath(getImagePath(content));

		// set up du titre
		idx = getFieldFlag(content, idx);
		if (idx == -1)
			return false;
		idx = setUpTitle(idx, content);

		// set up de la desc
		idx = getFieldFlag(content, idx);
		if (idx == -1)
			return false;
		setUpDesc(idx, content);

		return true;
	}

	private int getFieldFlag(String content, int idx) {
		boolean checked = false;
		while (idx + 32 < content.length() && !checked) {
			idx++;

			Character letter = content.charAt(idx);

			if (letter.toString().matches("[0-9A-F]")) {

				final String cc = content.substring(idx + 1, idx + 32);
				if (cc.matches("[0-9A-F]*")) {
					checked = true;
				} else {
					for (int i = 0; i < cc.length(); i++) {
						letter = cc.charAt(i);
						if (!letter.toString().matches("[0-9A-F]")) {
							idx += i;
							break;
						}
					}
				}

			}
		}
		if (checked)
			return idx;
		else
			return -1;
	}

	private int setUpTitle(int idx, String content) {
		idx += 37; // on avance du flag jusqu'au titre
		titlePos = idx;
		titleSize = 1;
		while (content.charAt(idx + 1) > 31) {
			idx++;
			titleSize++;
		}
		title = content.substring(titlePos, titlePos + titleSize);
		return idx;
	}

	private Integer setUpDesc(int idx, String content) {
		idx += 37;
		descPos = idx;
		descSize = 1;
		while (content.charAt(idx + 1) > 31) {
			idx++;
			descSize++;
		}
		desc = content.substring(descPos, descPos + descSize);
		return idx;
	}

	private String getImagePath(String content) {
		final int idxImage = content.indexOf("/Game/Textures/UI/ItemIcons/") + 28;
		int idxImageFin = idxImage;
		while (content.charAt(idxImageFin) > 31) {
			idxImageFin++;
		}
		return "UI" + File.separator + "ItemIcons" + File.separator + content.substring(idxImage, idxImageFin) + ".png";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTradDesc() {
		if (isDscTrnsltd())
			return tradDesc;
		else
			return getDesc();
	}

	public void setTradDesc(String tradDesc) {
		this.tradDesc = tradDesc;
	}

	public String getTradTitle() {
		if (isTtlTrnsltd())
			return tradTitle;
		else
			return getTitle();
	}

	public void setTradTitle(String tradTitle) {
		this.tradTitle = tradTitle;
	}

	public int getTitleSize() {
		return titleSize;
	}

	public void setTitleSize(int titleSize) {
		this.titleSize = titleSize;
	}

	public int getTitlePos() {
		return titlePos;
	}

	public void setTitlePos(int titlePos) {
		this.titlePos = titlePos;
	}

	public int getDescSize() {
		return descSize;
	}

	public void setDescSize(int descSize) {
		this.descSize = descSize;
	}

	public int getDescPos() {
		return descPos;
	}

	public void setDescPos(int descPos) {
		this.descPos = descPos;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public boolean isTtlTrnsltd() {
		return isTtlTrnsltd;
	}

	public boolean isDscTrnsltd() {
		return isDscTrnsltd;
	}

	public boolean checkTitle(String toCheck) {
		return this.titleSize >= toCheck.length() && StandardCharsets.US_ASCII.newEncoder().canEncode(toCheck);
	}

	public boolean checkDesc(String toCheck) {
		return this.descSize >= toCheck.length() && StandardCharsets.US_ASCII.newEncoder().canEncode(toCheck);
	}

	public boolean replaceTitle(String text) {
		if (this.title.equals(text)) {
			if (isTtlTrnsltd) {
				setTradTitle(null);
				isTtlTrnsltd = false;
				return true;
			} else
				return false;
		} else if (checkTitle(text)) {
			setTradTitle(text);
			isTtlTrnsltd = true;
			return true;
		}
		return false;
	}

	public boolean replaceDesc(String text) {
		if (this.desc.equals(text)) {
			if (isDscTrnsltd) {
				setTradDesc(null);
				isDscTrnsltd = false;
				return true;
			} else
				return false;
		} else if (checkDesc(text)) {
			setTradDesc(text);
			isDscTrnsltd = true;
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "ItemPickup [title=" + title + ", desc=" + desc + ", titleSize=" + titleSize + ", descSize=" + descSize
				+ ", titlePos=" + titlePos + ", descPos=" + descPos + ", tradTitle=" + tradTitle + ", tradDesc="
				+ tradDesc + ", imagePath=" + imagePath + "]";
	}

}
