/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.modele;

import java.util.ArrayList;
import java.util.List;

import fr.caoiva_suppliers.translator.filesManagment.Paker;
import fr.caoiva_suppliers.translator.filesManagment.Serializer;

public class Traducteur {

	private final List<Traduction> traductions;

	public Traducteur() {
		traductions = new ArrayList<>();
	}

	public List<Traduction> getTraductions() {
		return traductions;
	}

	public Traduction getTraduction(int idx) {
		return traductions.get(idx);
	}

	public void getSeralizedTrad() {
		for (final Traduction traduction : Serializer.getTrads()) {
			traduction.setSaved(true);
			addTraduction(Traduction.completeTrad(traduction));
		}
		System.out.println("serialized:" + traductions);
	}
	
	public void reloadSerialized() {
		traductions.clear();
		getSeralizedTrad();
	}

	public void addTraduction(Traduction trad) {
		traductions.add(trad);
		System.out.println("ajout traduction: " + trad);
	}

	public void writTrad(Traduction currentTrad) {
		final Traduction toWrite = Traduction.toWrite(currentTrad);
		Serializer.writeTrad(toWrite);
	}

	public void pak() {
		Paker.getInstance().pak();
	}

}
