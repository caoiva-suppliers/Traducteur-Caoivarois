/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.caoiva_suppliers.translator.filesManagment.FileManager;

@SuppressWarnings("serial")
public class Traduction implements Serializable {

	private List<ItemPickup> itemList;

	private String name;
	private transient boolean saved;

	public Traduction() {
		itemList = new ArrayList<>();
	}

	public boolean isTranslated() {
		for (ItemPickup itemPickup : itemList) {
			if (itemPickup.isTtlTrnsltd() || itemPickup.isDscTrnsltd())
				return true;
		}
		return false;
	}

	public int getTranslatedNumber() {
		int idx = 0;
		for (ItemPickup itemPickup : itemList) {
			if (itemPickup.isTtlTrnsltd() || itemPickup.isDscTrnsltd())
				idx++;
		}
		return idx;
	}

	public int getTitleTranslatedNumber() {
		int idx = 0;
		for (ItemPickup itemPickup : itemList) {
			if (itemPickup.isTtlTrnsltd())
				idx++;
		}
		return idx;
	}

	public int getDescTranslatedNumber() {
		int idx = 0;
		for (ItemPickup itemPickup : itemList) {
			if (itemPickup.isDscTrnsltd())
				idx++;
		}
		return idx;
	}

	public boolean loadIntern() {
		itemList = FileManager.getInstance().loadInternBlueprint();
		return true;
	}

	public List<ItemPickup> getItemList() {
		return itemList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean bool) {
		this.saved = bool;
	}

	public String toString() {
		return "Nom: " + name + " Nb d'item: " + itemList.size();
	}

	public static Traduction toWrite(Traduction currentTrad) {
		final Traduction res = new Traduction();
		res.name = currentTrad.name;
		for (ItemPickup entree : currentTrad.getItemList()) {
			if (entree.isTtlTrnsltd() || entree.isDscTrnsltd()) {
				res.itemList.add(entree);
			}
		}
		return res;
	}

	public static Traduction completeTrad(Traduction traduction) {
		final Traduction res = new Traduction();
		res.name = traduction.name;
		res.loadIntern();
		int idx = 0;
		final int max = traduction.itemList.size();
		for (int i = 0; i < res.itemList.size(); ++i) {
			ItemPickup entree = res.itemList.get(i);
			if (idx < max && entree.getName().equals(traduction.itemList.get(idx).getName())) {
				res.itemList.set(i, traduction.itemList.get(idx));
				idx++;
			}
		}
		return res;
	}

}
