/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.obsSubjectUtils;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    protected List<Observer> attached;

    public Subject() {
        attached = new ArrayList<>();
    }

    public void attach(Observer obs) {
        if (! attached.contains( obs)) {
            attached.add(obs);
        }
    }

    public void detach(Observer obs) {
        if (attached.contains( obs)) {
            attached.remove(obs);
        }
    }

    public void notifyObservers() {
        for (final Observer o : attached) {
            o.update(this);
        }
    }


    public void notifyObservers(Object data) {
        for (final Observer o : attached) {
            o.update(this, data);
        }
    }

}
