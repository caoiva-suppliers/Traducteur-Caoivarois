/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.view.callahan;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import fr.caoiva_suppliers.translator.modele.Traducteur;

public class CallahanView extends Stage {

	public CallahanView(Traducteur trad) throws IOException, InterruptedException {
		super();
		final FXMLLoader loader = new FXMLLoader();

		final TraducteurController controller;
		final Parent root;

		loader.setLocation(getClass().getResource("/fr/caoiva_suppliers/translator/view/callahan/Interface.fxml"));
		root = loader.load();

		controller = (TraducteurController) loader.getController();

		trad.getSeralizedTrad();
		controller.init(trad, root);
	}

}
