/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.view.callahan;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import fr.caoiva_suppliers.translator.modele.ItemPickup;

public class ItemListCell extends ListCell<ItemPickup> {

	private Node renderer;
	private ItemListCellController rendererControlleur;
	private final ItemListPageController page;

	public ItemListCell(ItemListPageController page) {
		super();
		// Chargement du FXML.
		this.page = page;
		try {
			final URL fxmlURL = getClass().getResource("/fr/caoiva_suppliers/translator/view/callahan/ListCell.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			renderer = (Node) fxmlLoader.load();
			rendererControlleur = (ItemListCellController) fxmlLoader.getController();
		} catch (IOException ex) {
			Logger.getLogger(ItemListCell.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

	@Override
	protected void updateItem(ItemPickup value, boolean empty) {
		super.updateItem(value, empty);
		String text = null;
		Node graphic = null;
		if (!empty && value != null) {
			// Si c'est un nombre premier, on utilise le nœud provenant du FXML.
			if (renderer != null) {
				graphic = renderer;
				rendererControlleur.setValue(value);
			} else {
				text = String.valueOf(value);
			}
		}
		setText(text);
		setGraphic(graphic);
		final MenuItem openItem = new MenuItem("Sélectionner");
		openItem.setOnAction(e -> {
			page.setMainItem(value);
		});
		final MenuItem renameItem = new MenuItem("Rétablir");
		renameItem.setOnAction(e -> {
			page.restablishItem(value);
		});
		final ContextMenu listMenu = new ContextMenu(openItem, renameItem);
		setContextMenu(listMenu);
	}
}
