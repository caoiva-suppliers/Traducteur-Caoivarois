/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.view.callahan;

import java.io.File;
import java.net.MalformedURLException;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import fr.caoiva_suppliers.translator.modele.ItemPickup;

public class ItemListCellController {

	@FXML
	private BorderPane container;

	@FXML
	private StackPane descField;

	@FXML
	private StackPane titleField;

	@FXML
	private TextArea descFld;

	@FXML
	private Label titleLbl;

	@FXML
	private Label descLbl;

	@FXML
	private ImageView imgViewer;

	@FXML
	private TextField titleFld;

	private final ChangeListener<ItemPickup> valueChangeListener = (
			ObservableValue<? extends ItemPickup> observableValue, ItemPickup oldValue, ItemPickup newValue) -> {
				// System.out.println(newValue.getTitle());
				updateUI(newValue);
			};

	private final Property<ItemPickup> value = new SimpleObjectProperty<>(this, "value");

	public ItemListCellController() {
		valueProperty().addListener(valueChangeListener);
	}

	public void initialize() {
		titleLbl.setText(null);
		descLbl.setText(null);
	}

	private void updateUI(ItemPickup newValue) {
		final ItemPickup val = newValue;

		titleLbl.setText(val.getTradTitle());
		descLbl.setText(val.getTradDesc());
		final File imgFile = new File("Images" + File.separator + "Textures" + File.separator + val.getImagePath());
		String path;
		try {
			path = imgFile.toURI().toURL().toString();
			final Image img = new Image(path);
			imgViewer.setImage(img);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}

	public final ItemPickup getValue() {
		return value.getValue();
	}

	public final void setValue(ItemPickup value) {
		this.value.setValue(value);
	}

	public final Property<ItemPickup> valueProperty() {
		return value;
	}
}
