/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.view.callahan;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import fr.caoiva_suppliers.translator.modele.ItemPickup;
import fr.caoiva_suppliers.translator.modele.Traduction;

public class ItemListPage {

	private Node node;
	private ItemListPageController controller;

	private Traduction traduction;
	private final TraducteurController mainController;

	public ItemListPage(TraducteurController mainController, Traduction traduction) {
		this.mainController = mainController;
		this.traduction = traduction;
		try {
			final URL fxmlURL = getClass()
					.getResource("/fr/caoiva_suppliers/translator/view/callahan/ItemListPage.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			node = (Node) fxmlLoader.load();
			controller = (ItemListPageController) fxmlLoader.getController();
			controller.init(this);
		} catch (IOException ex) {
			Logger.getLogger(ItemListCell.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
		}
	}

	public Node getNode() {
		return this.node;
	}

	public List<ItemPickup> getItems() {
		return traduction.getItemList();
	}

	public void setSavedStatus(boolean b) {
		traduction.setSaved(b);
	}

	public TraducteurController getMainController() {
		return mainController;
	}

	public void cancelTrad() {
		controller.cancelTrad();
	}

	public void refresh() {
		controller.refresh();
	}

	public void setTrad(Traduction trad) {
		this.traduction = trad;
		refresh();
	}

}
