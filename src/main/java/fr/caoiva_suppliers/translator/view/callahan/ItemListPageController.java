/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.view.callahan;

import java.io.File;
import java.net.MalformedURLException;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import fr.caoiva_suppliers.translator.modele.ItemPickup;

public class ItemListPageController {

	@FXML
	protected ListView<ItemPickup> itemList;

	@FXML
	private TextField mainItemTitleFld;

	@FXML
	private TextArea mainItemDescFld;

	@FXML
	private ImageView mainImgViewer;

	@FXML
	private Label mainItemTitleLbl, mainItemDescLbl, upperTip, lowerTip;

	@FXML
	private Pane itemSpacer1, itemSpacer2, itemSpacer3, itemSpacer4;

	private TraducteurController mainController;
	private ItemListPage itemListPage;

	private ItemPickup mainListItem;

	private boolean renaming, retexting;

	private static final int DOUBLE_CLICK_COUNT = 2;

	public void init(ItemListPage itemListPage) {
		this.itemListPage = itemListPage;
		this.mainController = this.itemListPage.getMainController();

		renaming = false;
		retexting = false;

		setCell();
		refresh();

		mainItemTitleFld.textProperty().addListener((obs, oldVal, newVal) -> {
			changeUpperTip();
		});
		mainItemDescFld.textProperty().addListener((obs, oldVal, newVal) -> {
			changeLowerTip();
		});

		mainController.widthProperty().addListener((obs, oldVal, newVal) -> {
			setItemSpace(newVal.doubleValue());
		});

	}

	private void selectItem(int selectedNumber) {
		itemList.requestFocus();
		itemList.getSelectionModel().select(selectedNumber);
		itemList.scrollTo(selectedNumber);
		setMainItem(itemList.getSelectionModel().getSelectedItem());
	}

	private void setCell() {
		itemList.setCellFactory((ListView<ItemPickup> lv) -> new ItemListCell(this));
		itemList.setOnMouseClicked(event -> { // permet la selection
			if (event.getClickCount() == DOUBLE_CLICK_COUNT) {
				setMainItem(itemList.getSelectionModel().getSelectedItem());
			}
		});
		itemList.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue,
					Boolean newPropertyValue) {
				if (newPropertyValue) {
					itemList.getItems();
				}
			}
		});
	}

	public void refresh() {
		int oldSelected = itemList.getSelectionModel().getSelectedIndex();

		if (!(oldSelected == -1)) {
			if (!itemListPage.getItems().get(oldSelected).getName()
					.equals(itemList.getItems().get(oldSelected).getName()))
				oldSelected = 0;
			selectItem(oldSelected);
		}
		
		itemList.getItems().clear();
		itemList.getItems().addAll(itemListPage.getItems());
	}

	/*
	 * Renommage et redéfinition de l'item sélectionné
	 */

	@FXML
	private void changeTitle(ActionEvent event) {
		renaming = true;
		enabledMainItemTitleField(true);
		mainItemTitleFld.setText(mainItemTitleLbl.getText());
		mainItemTitleFld.requestFocus();
		changeUpperTip();
	}

	@FXML
	private void testEndRename(KeyEvent event) {
		if (event.getCode().equals(KeyCode.ESCAPE)) {
			cancelRename();
		}
	}

	@FXML
	private void endRename(ActionEvent event) {
		if (mainListItem.replaceTitle(mainItemTitleFld.getText())) {
			itemListPage.setSavedStatus(false);
			renaming = false;

			refreshMainItem();
			itemList.refresh();
			mainController.refreshOuter();
		} else if (mainListItem.getTitle().equals(mainItemTitleFld.getText())) {
			cancelRename();
		}
	}

	@FXML
	private void changeDesc(ActionEvent event) {
		retexting = true;
		enabledMainItemDescField(true);
		mainItemDescFld.setText(mainItemDescLbl.getText());
		mainItemDescFld.requestFocus();
		changeLowerTip();
	}

	@FXML
	private void endRetext(KeyEvent event) {
		if (event.getCode().equals(KeyCode.ENTER)) {
			if (!event.isShiftDown() && !event.isControlDown()) {
				if (event.isConsumed()) {
					final int caretPosition = mainItemDescFld.getCaretPosition();
					mainItemDescFld.deleteText(caretPosition - 1, caretPosition);
				}
				if (mainListItem.checkDesc(mainItemDescFld.getText())) {
					retexting = false;
					itemListPage.setSavedStatus(false);

					mainListItem.replaceDesc(mainItemDescFld.getText());
					refreshMainItem();
					itemList.refresh();
					mainController.refreshOuter();
				} else if (mainListItem.getDesc().equals(mainItemDescFld.getText())) {
					cancelRetext();
				}
			} else {
				final int caretPosition = mainItemDescFld.getCaretPosition();
				mainItemDescFld.insertText(caretPosition, "\n");
			}
		} else if (event.getCode().equals(KeyCode.ESCAPE)) {
			cancelRetext();
		}
	}

	private void enabledMainItemTitleField(boolean enabled) {
		mainItemTitleFld.setVisible(enabled);
		mainItemTitleFld.setDisable(!enabled);
	}

	private void enabledMainItemDescField(boolean enabled) {
		mainItemDescFld.setVisible(enabled);
		mainItemDescFld.setDisable(!enabled);
	}

	public void cancelTrad() {
		if (renaming)
			cancelRename();
		if (retexting)
			cancelRetext();
	}

	private void cancelRename() {
		mainItemTitleFld.setVisible(false);
		mainItemTitleFld.setDisable(true);
		renaming = false;
		setTips();
	}

	private void cancelRetext() {
		mainItemDescFld.setVisible(false);
		mainItemDescFld.setDisable(true);
		retexting = false;
		setTips();
	}

	private void setTips() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (mainListItem.isTtlTrnsltd()) {
					final int length = mainListItem.getTradTitle().length();
					setUpperTip(length, mainListItem.getTitleSize());
				} else
					clearUpperTip();
				if (mainListItem.isDscTrnsltd()) {
					final int length = mainListItem.getTradDesc().length();
					setLowerTip(length, mainListItem.getDescSize());
				} else
					clearLowerTip();
			}
		});
	}

	private void changeUpperTip() {
		final int length = mainItemTitleFld.getText().length();
		setUpperTip(length, mainListItem.getTitleSize());
	}

	private void setUpperTip(int field, int title) {
		upperTip.setText("Nombre de caractères : " + field + "/" + title);
		if (field > title) {
			upperTip.setStyle("-fx-text-fill:red;");
		} else {
			upperTip.setStyle("");
		}
	}

	private void clearUpperTip() {
		upperTip.setText("");
	}

	private void changeLowerTip() {
		final int length = mainItemDescFld.getText().length();
		setLowerTip(length, mainListItem.getDescSize());
	}

	private void setLowerTip(int field, int title) {
		if (field > title) {
			lowerTip.setStyle("-fx-text-fill:red;");
		} else {
			lowerTip.setStyle("");
		}
		lowerTip.setText("Nombre de caractères : " + field + "/" + title
				+ (field > title ? "\nUn saut à la ligne compte pour 2 !!" : ""));
	}

	private void clearLowerTip() {
		lowerTip.setText("");
	}

	private void setItemSpace(double newVal) {
		final double panelSizeLeft = ((newVal / 2) - 451) / 2;
		final double panelSizeRight = ((newVal / 2) - 407) / 2;
		itemSpacer1.setPrefWidth(panelSizeLeft);
		itemSpacer2.setPrefWidth(panelSizeLeft);
		itemSpacer3.setPrefWidth(panelSizeRight);
		itemSpacer4.setPrefWidth(panelSizeRight);
	}

	public void setMainItem(ItemPickup itemPickup) {
		cancelTrad();
		mainListItem = itemPickup;
		refreshMainItem();
	}

	private void refreshMainItem() {
		setMainItemImage();
		enabledMainItemTitleField(false);
		enabledMainItemDescField(false);
		mainItemTitleLbl.setText(mainListItem.getTradTitle());
		mainItemDescLbl.setText(mainListItem.getTradDesc());
		setTips();
	}

	private void setMainItemImage() {
		final File imgFile = new File(
				"Images" + File.separator + "Textures" + File.separator + mainListItem.getImagePath());
		String path;
		try {
			path = imgFile.toURI().toURL().toString();
			final Image img = new Image(path);
			mainImgViewer.setImage(img);
		} catch (MalformedURLException e) {
			System.out.println("Pb avec l'image de l'item sélectionné");
			e.printStackTrace(); // TODO Logger
		}
	}

	public void restablishItem(ItemPickup value) {
		if (value.isTtlTrnsltd()) {
			value.replaceTitle(value.getTitle());
			itemListPage.setSavedStatus(false);
		}
		if (value.isDscTrnsltd()) {
			value.replaceDesc(value.getDesc());
			itemListPage.setSavedStatus(false);
		}
		if (mainListItem.equals(value)) {
			cancelTrad();
			refreshMainItem();
		}
		itemList.refresh();
		mainController.refreshOuter();
	}

}
