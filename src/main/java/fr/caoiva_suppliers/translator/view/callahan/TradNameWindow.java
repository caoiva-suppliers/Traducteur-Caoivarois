/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.view.callahan;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.PopupWindow;
import javafx.stage.Stage;

public class TradNameWindow extends Stage {

	private final TextField tf;
	private final Tooltip tt;
	private final TraducteurController cont;

	public TradNameWindow(TraducteurController cont) {
		super();
		this.cont = cont;

		final AnchorPane pane = new AnchorPane();
		pane.setPrefHeight(150);
		pane.setPrefWidth(400);

		final Label title = new Label("Votre Traduction n'a pas de nom !");
		title.setLayoutX(65);
		title.setLayoutY(14);
		title.setFont(new Font(18));

		final Label subTitle = new Label("Veuillez en saisir un :");
		subTitle.setLayoutX(131);
		subTitle.setLayoutY(41);
		subTitle.setFont(new Font(15));

		tf = new TextField();
		tf.setPromptText("Nom de la traduction");
		tf.setFont(new Font(12));
		tf.setLayoutX(87);
		tf.setLayoutY(75);
		tf.setPrefWidth(225);

		tt = new Tooltip("Pas de \\|.<>\"*");
		tt.setAnchorLocation(PopupWindow.AnchorLocation.WINDOW_TOP_LEFT);
		tt.setAutoHide(false);
		Button validate, cancel;
		validate = new Button("Valider");
		cancel = new Button("Annuler");
		validate.setLayoutX(105);
		validate.setLayoutY(111);
		cancel.setLayoutX(243);
		cancel.setLayoutY(111);
		this.xProperty().addListener((obs, oldVal, newVal) -> {
			if (tt.isShowing()) {
				showTooltip();
			}
		});
		this.yProperty().addListener((obs, oldVal, newVal) -> {
			if (tt.isShowing()) {
				showTooltip();
			}
		});
		validate.setOnAction(e -> {
			if (checkText(tf.getText())) {
				this.cont.saveAs(tf.getText());
				this.close();
			} else if (!tt.isShowing()) {
				showTooltip();
			}
		});

		cancel.setOnAction(e -> this.close());

		pane.getChildren().add(title);
		pane.getChildren().add(subTitle);
		pane.getChildren().add(tf);
		pane.getChildren().add(validate);
		pane.getChildren().add(cancel);

		this.setScene(new Scene(pane));
		this.setResizable(false);
		this.setTitle("Veuillez nommer cette Traduction");
		this.showAndWait();
	}

	private boolean checkText(String text) {
		if (text == null || text.equals(""))
			return false;

		if (text.matches(".*[\\\\/\\.!?*:<>|\"].*")) {
			return false;
		}
		return true;
	}

	private void showTooltip() {
		tt.show(this, this.getX() + 199.5 - 39.5, this.getY() + 134);
	}

}
