/**
 *	Caoish translator
 *
 *  Copyright 2021 by Sylvain Camus.
 *  
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See Licences.
 *  
 *  @license GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
 */

package fr.caoiva_suppliers.translator.view.callahan;

import fr.caoiva_suppliers.translator.App;
import fr.caoiva_suppliers.translator.filesManagment.FileManager;
import fr.caoiva_suppliers.translator.filesManagment.PathSelector;
import fr.caoiva_suppliers.translator.modele.Traducteur;
import fr.caoiva_suppliers.translator.modele.Traduction;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TraducteurController extends Stage {

	@FXML
	private Tab techTab;

	@FXML
	private Tab itemsTab;

	private ItemListPage itemListPage;

	@FXML
	private VBox itemPane;

	@FXML
	private Tab vehiclesTab;

	@FXML
	private Tab structuresTab;

	@FXML
	private Label leftStatus, rightStatus;

	@FXML
	private Tooltip leftTooltip, rightTooltip;

	@FXML
	private Menu tradMenu;

	private Traducteur traducteur;
	private Traduction currentTrad;
	private PathSelector ps;

	public void init(Traducteur traducteur, Parent root) {
		// mainTabs.getSelectionModel().clearAndSelect(0);

		if (traducteur != null)
			this.traducteur = traducteur;
		else
			this.traducteur = new Traducteur();

		initTraductions();

		ps = new PathSelector();

		initTab();

		this.setScene(new Scene(root));
		this.setResizable(true);
		this.show();
	}

	private void initTraductions() {
		if (this.traducteur.getTraductions().size() == 0)
			setMainTrad(getNewTrad());
		else
			setMainTrad(this.traducteur.getTraductions().get(0));
	}

	private void initTab() {
		itemListPage = new ItemListPage(this, currentTrad);
		itemsTab.setContent(itemListPage.getNode());
	}

	public void reloadTraductions() {
		traducteur.reloadSerialized();
		initTraductions();
		initTab();
	}

	public void refreshTabs() {
		itemListPage.refresh();
	}

	private void setTraductionMenu() {
		setNewTradButton();
		final int maxTaille = 6;
		final int taille = traducteur.getTraductions().size();
		int idx = 0;

		if (taille < 2 && currentTrad.equals(traducteur.getTraduction(idx))) {
			final MenuItem empty = new MenuItem("Aucune autre Traduction");
			empty.setDisable(true);
			tradMenu.getItems().add(empty);
		} else {

			while (idx < taille && idx < maxTaille) {
				if (!currentTrad.equals(traducteur.getTraduction(idx))) {
					addTradMenuItem(traducteur.getTraduction(idx), tradMenu);
				}
				++idx;
			}

			if (taille == maxTaille)
				addTradMenuItem(traducteur.getTraduction(idx), tradMenu);
			else if (taille > maxTaille) {
				final Menu otherMenu = new Menu("Autres Traductions...");
				tradMenu.getItems().add(otherMenu);
				while (idx < taille) {
					if (!currentTrad.equals(traducteur.getTraduction(idx))) {
						addTradMenuItem(traducteur.getTraduction(idx), otherMenu);
					}
					++idx;
				}
			}
		}
	}

	private void addTradMenuItem(Traduction trad, Menu menu) {
		if (trad.isTranslated() || trad.getName() != null) {
			final MenuItem newI = new MenuItem(trad.getName());
			newI.setOnAction(e -> {
				changeMainTrad(trad);

			});
			menu.getItems().add(newI);
		}
	}

	private void changeMainTrad(Traduction trad) {
		saveRequest(new ActionEvent());
		if (!currentTrad.isSaved() || currentTrad.getName() == null) {
			final int idx = traducteur.getTraductions().indexOf(currentTrad);
			if (idx != -1)
				traducteur.getTraductions().remove(idx);
		}
		setMainTrad(trad);
		itemListPage.setTrad(trad);
	}

	private void setNewTradButton() {
		final MenuItem neww = new MenuItem("Nouvelle Traduction");
		neww.setOnAction(e -> {
			changeMainTrad(getNewTrad());
		});
		tradMenu.getItems().add(neww);
		tradMenu.getItems().add(new SeparatorMenuItem());
	}

	private Traduction getNewTrad() {
		final Traduction nouvelle = new Traduction();
		nouvelle.loadIntern();
		traducteur.addTraduction(nouvelle);
		return nouvelle;
	}

	@FXML
	public void saveRequest(ActionEvent event) {
		if (currentTrad.getName() == null) {
			if (currentTrad.isTranslated())
				new TradNameWindow(this);
		} else if (("").equals(currentTrad.getName()))
			new TradNameWindow(this);
		else
			save();
	}

	@FXML
	public void saveAsRequest(ActionEvent event) {
		new TradNameWindow(this);
	}

	public void saveAs(String name) {
		currentTrad.setName(name);
		save();
	}

	private void save() {
		cancelTrad();
		traducteur.writTrad(currentTrad);
		currentTrad.setSaved(true);
		refreshOuter();
	}

	private void cancelTrad() {
		itemListPage.cancelTrad();
	}

	public void setMainTrad(Traduction trad) {
		this.currentTrad = trad;
		currentTrad.setSaved(true);
		refreshOuter();
		tradMenu.getItems().clear();
		setTraductionMenu();
	}

	public void refreshOuter() {
		setTitle();
		setTranslatedNumber();
	}

	private void setTitle() {
		String display = "";
		if (currentTrad.getName() != null && !currentTrad.getName().equals(""))
			display = " - " + currentTrad.getName() + (currentTrad.isSaved() ? "" : " *");
		this.setTitle("Traducteur Caoivarois" + display);
		leftStatus.setText(currentTrad.getName());
	}

	private void setTranslatedNumber() {
		rightStatus.setText((currentTrad.isSaved() ? "" : "* ") + "nb d'objets traduit : "
				+ currentTrad.getTranslatedNumber() + "/" + currentTrad.getItemList().size());
		rightTooltip.setText((currentTrad.isSaved() ? "" : "(non enregistré)\n") + "Dont :\n - "
				+ currentTrad.getTitleTranslatedNumber() + " titres\n - " + currentTrad.getDescTranslatedNumber()
				+ " descriptions");
	}

	public void repathRequest(ActionEvent event) {
		ps.pathSelector(this);
		// TODO new Thread ?
	}

	@FXML
	private void exportUassetRequest(ActionEvent event) {
		FileManager.getInstance().writeFiles(currentTrad);
		// TODO new thread ?
	}

	@FXML
	private void exportPakRequest(ActionEvent event) {
		// TODO export .pak
	}

	@FXML
	public void about() {
		App.getInstance().about();
	}

	public Traduction getCurrentTraduction() {
		return currentTrad;
	}

}
